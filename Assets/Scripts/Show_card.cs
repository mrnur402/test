using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
[Serializable]
public class card 
{
    public GameObject back;
    public GameObject top;
    public bool win;
    public bool opened;
    public int id;
}

public class Show_card : MonoBehaviour
{
    int opened = 0;
    [SerializeField] public List<card> main_obj;
    [SerializeField] Text score_txt;
    [SerializeField] Text time_txt;
    [SerializeField] RectTransform grid1;
    [SerializeField] RectTransform grid2;
    [SerializeField] RectTransform exit;
    [SerializeField] int time = 60;
    public int score = 0;
    private void Start()
    {
        time_txt.text = "LEFT TIME: <color=green>" + time + "</color>";
        StartCoroutine(show_all(3f));
        StartCoroutine(timer());
    }
    IEnumerator timer()
    {
        yield return new WaitForSeconds(1f);
        time -= 1;
        time_txt.text = "LEFT TIME: <color=green>" + time + "</color>";
        if (time == 0)
            dead();
        else
            StartCoroutine(timer());
    }
    void dead()
    {
        if (score > PlayerPrefs.GetInt("Best"))
            PlayerPrefs.SetInt("Best", score);
        time_txt.text = "BEST SCORE: <color=green>" + PlayerPrefs.GetInt("Best") + "</color>";
        exit.DOScale(new Vector3(0, 0, 0), 0f);
        exit.gameObject.SetActive(true);
        exit.DOScale(new Vector3(1, 1, 1), 1f);
        score_txt.rectTransform.DOLocalMoveY(200, 1f);
        time_txt.rectTransform.DOLocalMoveY(0, 1f);
        grid1.DOMoveY(-2000, 1f);
        grid2.DOMoveY(-2000, 1f);
    }
    public void show(int num)
    {
        if (opened != 2)
        {
            main_obj[num].back.GetComponent<RectTransform>().DORotate(new Vector3(0, 90, 0), 0.5f);
            main_obj[num].top.GetComponent<RectTransform>().DORotate(new Vector3(0, -180, 0), 1f);
            main_obj[num].opened = true;
        }
        opened++;
        if (opened == 2)
        {
            check();
        }
    }
    void check()
    {
        int first_id = -1;
        foreach (card a in main_obj)
        {
            if (a.opened == true && !a.win)
            {
                if (first_id == -1)
                {
                    first_id = a.id;
                }
                else
                {
                    if (first_id == a.id)
                    {
                        foreach (card b in main_obj)
                        {
                            if (b.opened && !b.win)
                            {
                                b.win = true;
                            }
                        }
                        if (check_finish())
                            StartCoroutine(spawn());
                        score++;
                        score_txt.text = "SCORE: <color=green>" + score + "</color>";
                        opened = 0;
                        break;
                    }
                    else
                    {
                        StartCoroutine(hide_all(1.5f));
                        break;
                    }
                }
            }
        }
    }
    IEnumerator spawn()
    {
        yield return new WaitForSeconds(1f);
        foreach (card a in main_obj)
        {
            a.back.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.5f);
            a.top.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 1f);
            a.opened = false;
            a.win = false;
        }
        opened = 0;
        yield return new WaitForSeconds(1f);
        GetComponent<Spawner>().spawn();
        StartCoroutine(show_all(3f));
    }
    bool check_finish() {
        foreach (card b in main_obj)
        {
            if (!b.win)
                return false;
        }
        return true;
    }
    IEnumerator hide_all(float time)
    {
        yield return new WaitForSeconds(time);
        foreach (card a in main_obj)
        {
            if (!a.win)
            {
                a.back.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.5f);
                a.top.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 1f);
                a.opened = false;
            }
        }
        opened = 0;
    }
    IEnumerator show_all(float time)
    {
        foreach (card a in main_obj)
        {
            if (!a.win)
            {
                a.back.GetComponent<RectTransform>().DORotate(new Vector3(0, 90, 0), 0.5f);
                a.top.GetComponent<RectTransform>().DORotate(new Vector3(0, -180, 0), 1f);
                a.opened = false;
            }
        }
        opened = 0;
        yield return new WaitForSeconds(time);
        StartCoroutine(hide_all(time));
    }
}