using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[Serializable]
public class sprites
{
    public Sprite img;
    public int id=0;
    public int count =0;
}
public class Spawner : MonoBehaviour
{
    [SerializeField] List<sprites> spawn_obj;
    [SerializeField] List<Image> img;
    void Awake()
    {
        spawn();
    }
    public void spawn()
    {
        List<sprites> save = new List<sprites>();
        foreach (sprites a in spawn_obj)
        {
            a.count = 0;
            save.Add(a);
        }
        for (int i = 0; i < img.Count; i++)
        {
            int rnd = UnityEngine.Random.Range(0, spawn_obj.Count);
            img[i].sprite = spawn_obj[rnd].img;
            GetComponent<Show_card>().main_obj[i].id = spawn_obj[rnd].id;
            spawn_obj[rnd].count++;
            if (spawn_obj[rnd].count == 2)
            {
                spawn_obj.RemoveAt(rnd);
            }
        }
        foreach (sprites a in save)
        {
            spawn_obj.Add(a);
        }
    }
}