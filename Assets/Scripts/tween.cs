using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
public class tween : MonoBehaviour
{
    [SerializeField] Button start;
    [SerializeField] Button back;
    [SerializeField] Text best;
    [SerializeField] GameObject canvas_start;
    [SerializeField] GameObject canvas_game;
    private void Awake()
    {
        GetComponent<Spawner>().enabled = false;
        GetComponent<Show_card>().enabled = false;
    }
    private void Start()
    {
        best.text = "BEST SCORE: <color=green>" + PlayerPrefs.GetInt("Best") + "</color>";
        start.onClick.AddListener(open_game);
        back.onClick.AddListener(close_game);
    }
    void open_game()
    {
        GetComponent<Spawner>().enabled = true;
        GetComponent<Show_card>().enabled = true;
        back.gameObject.SetActive(false);
        canvas_start.transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        canvas_game.transform.DOScale(new Vector3(0, 0, 0), 0f);
        canvas_game.gameObject.SetActive(true);
        canvas_game.transform.DOScale(new Vector3(1, 1, 1), 1f);
    }
    void close_game()
    {
        canvas_start.transform.DOScale(new Vector3(1, 1, 1), 1f);
        canvas_game.transform.DOScale(new Vector3(0, 0, 0), 1f);
        best.text = "BEST SCORE: <color=green>" + PlayerPrefs.GetInt("Best") + "</color>";
        StartCoroutine(reload());
    }
    IEnumerator reload() {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }
}
